package com.jakubkolacz.cryptoexchange.service

import com.jakubkolacz.cryptoexchange.domain.model.CryptoCurrency
import com.jakubkolacz.cryptoexchange.repository.CryptoCurrencyJpaRepository
import com.jakubkolacz.cryptoexchange.service.crypto.impl.CryptoCurrencyServiceImpl
import spock.lang.Specification

class CryptoCurrencyServiceImplSpec extends Specification {
    def cryptoService
    def cryptoCurrencyRepository = Mock(CryptoCurrencyJpaRepository.class)

    def setup() {
        cryptoService = new CryptoCurrencyServiceImpl(cryptoCurrencyRepository)
    }

    def 'should save crypto currency'() {
        given:
        CryptoCurrency currency = new CryptoCurrency(1L, "name", Collections.emptyList())

        when:
        cryptoService.save(currency)

        then:
        1 * cryptoCurrencyRepository.save(currency) >> currency
        1 * cryptoCurrencyRepository.findById(currency.id) >> Optional.empty()
        0 * _
    }

    def 'should get cryptocurrency when exist'() {
        when:
        cryptoService.findById(1L)

        then:
        1 * cryptoCurrencyRepository.findById(1L) >> Optional.of(new CryptoCurrency())
        0 * _
    }

    def 'should return empty optional when currency does not exist'() {

        when:
        cryptoService.findById(1L)

        then:
        1 * cryptoCurrencyRepository.findById(1L) >> Optional.empty()
        0 * _
    }

    /*this should be working but idk why it does not*/
//    def 'should update currency when exist and correct args are passed'(){
//        given:
//        List<Price> list = Collections.emptyList()
//        def currency= new CryptoCurrency(1L, "test name", list)
//
//        when:
//        cryptoService.update(1L, new CryptoCurrency(2L, "example name", list))
//
//
//         then:
//        1 * cryptoCurrencyRepository.findById(1L) >> Optional.of(currency)
//        1 * currency.setId()
//        1 * currency.setName()
//        1 * currency.setPriceInTime()
//        0 * _
//    }

    def 'should delete currency when exist'() {
        when:
        cryptoService.deleteById(1L)

        then:
        1 * cryptoCurrencyRepository.deleteById(1L)
        0 * _
    }
}