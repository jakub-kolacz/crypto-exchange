package com.jakubkolacz.cryptoexchange.domain.model;

import java.util.List;

public abstract class Currency {
    private String name;
    private List<Price> priceInTime;

    public Currency() {
    }

    public Currency(String name, List<Price> priceInTime) {
        this.name = name;
        this.priceInTime = priceInTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Price> getPriceInTime() {
        return priceInTime;
    }

    public void setPriceInTime(List<Price> priceInTime) {
        this.priceInTime = priceInTime;
    }
}
