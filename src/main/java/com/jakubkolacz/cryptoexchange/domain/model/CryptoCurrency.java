package com.jakubkolacz.cryptoexchange.domain.model;

import lombok.Builder;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
public class CryptoCurrency extends Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany
    private List<Price> priceInTime;

    public CryptoCurrency() {
    }

    public CryptoCurrency(Long id, String name, List<Price> priceInTime) {
        this.id = id;
        this.name = name;
        this.priceInTime = priceInTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Price> getPriceInTime() {
        return priceInTime;
    }

    public void setPriceInTime(List<Price> priceInTime) {
        this.priceInTime = priceInTime;
    }
}
