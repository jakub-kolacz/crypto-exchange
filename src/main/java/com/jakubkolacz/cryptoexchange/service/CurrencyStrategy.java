package com.jakubkolacz.cryptoexchange.service;

public interface CurrencyStrategy<T, S> {
    T save(S currency);

    T findById(Long id);

    S update(Long id, S currency);

    void deleteById(Long id);
}
