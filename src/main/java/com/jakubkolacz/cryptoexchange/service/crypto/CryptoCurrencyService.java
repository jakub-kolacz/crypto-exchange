package com.jakubkolacz.cryptoexchange.service.crypto;

import com.jakubkolacz.cryptoexchange.domain.model.CryptoCurrency;
import com.jakubkolacz.cryptoexchange.service.CurrencyStrategy;

import javax.transaction.Transactional;
import java.util.Optional;

public interface CryptoCurrencyService extends CurrencyStrategy<Optional<CryptoCurrency>, CryptoCurrency> {

    @Override
    Optional<CryptoCurrency> save(CryptoCurrency currency);

    Optional<CryptoCurrency> findById(Long id);

    @Override
    @Transactional
    CryptoCurrency update(Long id, CryptoCurrency currency);

    @Override
    void deleteById(Long id);
}
