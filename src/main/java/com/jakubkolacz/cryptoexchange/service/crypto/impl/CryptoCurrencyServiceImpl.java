package com.jakubkolacz.cryptoexchange.service.crypto.impl;

import com.jakubkolacz.cryptoexchange.domain.model.CryptoCurrency;
import com.jakubkolacz.cryptoexchange.repository.CryptoCurrencyJpaRepository;
import com.jakubkolacz.cryptoexchange.service.crypto.CryptoCurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CryptoCurrencyServiceImpl implements CryptoCurrencyService {
    private final CryptoCurrencyJpaRepository cryptoCurrencyJPARepository;

    @Override
    public Optional<CryptoCurrency> save(CryptoCurrency currency) {
        if (cryptoCurrencyJPARepository.findById(currency.getId()).isPresent()) {
            throw new EntityExistsException("Currency already exists");
        }
        return Optional.of(cryptoCurrencyJPARepository.save(currency));
    }

    @Override
    public Optional<CryptoCurrency> findById(Long id) {
        return cryptoCurrencyJPARepository.findById(id);
    }

    @Override
    public CryptoCurrency update(Long id, CryptoCurrency currency) {
        CryptoCurrency currencyDb = findById(id).get();
        currencyDb.setId(currency.getId());
        currencyDb.setName(currency.getName());
        currencyDb.setPriceInTime(currency.getPriceInTime());
        return currencyDb;
    }

    @Override
    public void deleteById(Long id) {
        cryptoCurrencyJPARepository.deleteById(id);
    }
}
