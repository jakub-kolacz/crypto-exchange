package com.jakubkolacz.cryptoexchange.repository;

import com.jakubkolacz.cryptoexchange.domain.model.CryptoCurrency;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CryptoCurrencyJpaRepository extends JpaRepository<CryptoCurrency, Long> {
}
