package com.jakubkolacz.cryptoexchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class CryptoexchangeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CryptoexchangeApplication.class, args);
    }

}
